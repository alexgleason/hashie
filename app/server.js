const hashie = require("./hashie")

// Automatically join rooms when invited
hashie.onInvited(function(roomId) {
  hashie.joinRoom(roomId)
})

// Handle incoming messages
hashie.onMessage(function(roomId, message) {

  // Set the room title to a hashtag if someone writes a hashtag on its own
  if (message.match(/^#\S*$/i)) {
    hashie.setRoomName(roomId, message)
  }
})

hashie.start()
