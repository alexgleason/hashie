const matrixSdk = require('matrix-js-sdk')

/*******************************************************************************
* Matrix connect
*******************************************************************************/

const hashieUserId = '@hashie:matrix.org'

var matrixClient = matrixSdk.createClient({
    baseUrl: 'https://matrix.org',
    accessToken: process.env['MATRIX_ACCESS_TOKEN'],
    userId: hashieUserId
})

/*******************************************************************************
* Core functions
*******************************************************************************/

// Messages any Matrix room
function sendToRoom(roomId, message) {
  // 2 second delay in messages so it shows Hashie "typing"
  const typingTime = 2000
  matrixClient.sendTyping(roomId, true, typingTime, function() {
    setTimeout(function() {
      matrixClient.sendMessage(roomId, {
        "msgtype": "m.text",
        "body": message
      })
      matrixClient.sendTyping(roomId, false)
    }, typingTime)
  })
}

// Sets a room's name
function setRoomName(roomId, name) {
  matrixClient.setRoomName(roomId, name)
}

// Joins any Matrix room
function joinRoom(roomId) {
  matrixClient.joinRoom(roomId).done(function() {
    sendToRoom(roomId, "Hello world!")
  })
}

// Provides a callback to handle incoming messages
function onMessage(callback) {
  matrixClient.once('sync', function(state, prevState) {
    if (state === 'PREPARED') {
      matrixClient.on("Room.timeline", function(event, room, toStartOfTimeline) {
        if (toStartOfTimeline || event.getSender() === hashieUserId) {
          return // Ignore Hashie's own messages
        }
        if (event.getType() !== "m.room.message") {
          return // Only respond to normal messages
        }
        callback(room.roomId, event.getContent().body)
      })
    }
  })
}

// Provides a callback to handle room invitations
function onInvited(callback) {
  matrixClient.on("RoomMember.membership", function(event, member) {
    if (member.membership === "invite" && member.userId === hashieUserId) {
      callback(member.roomId)
    }
  })
}

// Starts Hashie
function start() {
  matrixClient.startClient()
}

/*******************************************************************************
* Export as a module
*******************************************************************************/

module.exports = {
  hashieUserId: hashieUserId,

  setRoomName: setRoomName,
  onMessage: onMessage,
  onInvited: onInvited,
  joinRoom: joinRoom,
  start: start
}
