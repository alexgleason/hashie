# Hashie, the hashtag robot

This is a dead simple Matrix.org chatbot who listens for messages in the form of hashtags and then sets the room title to that message.

For instance, if you enter `#HellYeah` as a standalone message with nothing else, hashie will set the room title to `#HellYeah`.

You can invite `@hashie:matrix.org` into your room to enable this functionality. You must make him a moderator of the room according to the default settings for this to work.
